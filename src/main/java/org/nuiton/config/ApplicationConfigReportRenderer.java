package org.nuiton.config;

/*
 * #%L
 * Nuiton Maven Report Plugin
 * %%
 * Copyright (C) 2012 - 2016 CodeLutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.commons.collections.CollectionUtils;
import org.apache.maven.doxia.sink.Sink;
import org.apache.maven.doxia.sink.SinkEventAttributeSet;
import org.apache.maven.doxia.sink.SinkEventAttributes;
import org.apache.maven.reporting.AbstractMavenReportRenderer;
import org.codehaus.plexus.i18n.I18N;
import org.codehaus.plexus.util.StringUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import static org.nuiton.i18n.I18n.l;

/**
 * Render the {@link ApplicationConfigReport} report.
 *
 * @author Tony - chemit@codelutin.com
 * @since 2.6.10
 */
public class ApplicationConfigReportRenderer extends AbstractMavenReportRenderer {

    /**
     * Internationalization component.
     *
     * @since 2.6.10
     */
    protected final I18N i18n;

    /**
     * The locale we are rendering for.
     *
     * @since 2.6.10
     */
    protected final Locale locale;

    /**
     * The report physical name (used to generated link).
     *
     * @since 3.0
     */
    protected final String reportName;

    /**
     * The name of the bundle containing our I18n resources.
     *
     * @since 2.6.10
     */
    protected final String bundleName;

    /**
     * Set of config providers to generate.
     *
     * @since 2.6.10
     */
    protected final Set<ApplicationConfigProvider> configProviders;


    /**
     * To show option in detail.
     *
     * @since 2.6.10
     */
    protected final boolean optionWithDetail;

    public ApplicationConfigReportRenderer(Sink sink,
                                           I18N i18n,
                                           Locale locale,
                                           String reportName,
                                           String bundleName,
                                           Set<ApplicationConfigProvider> configProviders,
                                           boolean optionWithDetail) {
        super(sink);
        this.i18n = i18n;
        this.locale = locale;
        this.reportName = reportName;
        this.bundleName = bundleName;
        this.sink = sink;
        this.configProviders = configProviders;
        this.optionWithDetail = optionWithDetail;
    }

    @Override
    public String getTitle() {
        return getText("report.title");
    }

    @Override
    public void renderBody() {

        sink.section1();
        sink.sectionTitle1();
        sink.text(getText("report.overview.title"));
        sink.sectionTitle1_();
        sink.lineBreak();
        sink.paragraph();
        sink.text(getText("report.overview.text"));
        sink.paragraph_();

        sink.paragraph();
        sink.link("http://doc.nuiton.org/nuiton-config/index.html");
        sink.text(getText("report.overview.more.information"));
        sink.link_();
        sink.paragraph_();
        sink.lineBreak();

        renderProviderSummaryTable();

        sink.section1_();

        if (CollectionUtils.isNotEmpty(configProviders)) {

            sink.section1();
            sink.sectionTitle1();
            sink.text(getText("report.detail.title"));
            sink.sectionTitle1_();
            sink.lineBreak();
            sink.paragraph();
            sink.text(getText("report.detail.text"));
            sink.paragraph_();

            for (ApplicationConfigProvider configProvider : configProviders) {

                renderConfigProviderDetail(configProvider);
            }

            sink.section1_();
        }
    }

    protected void renderProviderSummaryTable() {

        if (CollectionUtils.isEmpty(configProviders)) {

            sink.text(getText("report.detail.text.noConfig"));

        } else {
            sink.table();

            sink.tableRow();
            sinkHeaderCellText(getText("report.config.name"));
            sinkHeaderCellText(getText("report.config.description"));
            sinkHeaderCellText(getText("report.config.nbOptions"));
            sinkHeaderCellText(getText("report.config.nbActions"));
            sink.tableRow_();

            for (ApplicationConfigProvider configProvider : configProviders) {

                sink.tableRow();
                sinkCellLink(configProvider.getName(), "detail_" + configProvider.getName());
                sinkCellText(configProvider.getDescription(locale));
                sinkCellText(configProvider.getOptions().length + "");
                sinkCellText(configProvider.getActions().length + "");
                sink.tableRow_();
            }
            sink.table_();
        }
    }

    protected void renderConfigProviderDetail(ApplicationConfigProvider configProvider) {

        sink.section2();
        sink.sectionTitle2();
        sink.anchor("detail_" + configProvider.getName());
        sink.text(getText("report.detail.configuration.title") + "   " +
                  configProvider.getName());
        sink.anchor_();
        sink.sectionTitle2_();

        sink.lineBreak();
        sink.paragraph();
        sink.text(getText("report.config.name") + " : ");
        sink.bold();
        sink.text(configProvider.getName());
        sink.bold_();
        sink.paragraph_();

        sink.lineBreak();
        sink.paragraph();
        sink.text(getText("report.config.description") + " : ");
        sink.bold();
        sink.text(configProvider.getDescription(locale));
        sink.bold_();
        sink.paragraph_();

        sink.section3();
        sink.sectionTitle3();
        sink.anchor("detail_options_" + configProvider.getName());

        sink.text(getText("report.detail.options.title"));
        sink.anchor_();
        sink.sectionTitle3_();
        sink.lineBreak();

        Collection<ConfigOptionDef> options =
                getOptions(configProvider);

        renderOptionDefsTable(configProvider, options);

        if (optionWithDetail) {

            for (ConfigOptionDef option : options) {

                renderProviderOptionDetail(configProvider, option);
            }
        }

        sink.section3_();

        sink.section3();
        sink.sectionTitle3();
        sink.text(getText("report.detail.actions.title"));
        sink.sectionTitle3_();
        sink.lineBreak();

        renderActionDefsTable(configProvider.getActions());

        sink.section3_();

        sink.section2_();
    }

    protected void renderOptionDefsTable(ApplicationConfigProvider configProvider,
                                         Collection<ConfigOptionDef> options) {

        if (options.isEmpty()) {

            sink.paragraph();
            sink.bold();
            sink.text(getText("report.detail.options.noOptions"));
            sink.bold_();
            sink.paragraph_();

        } else {

            sink.table();
            sink.tableRow();

            sinkHeaderCellText(getText("report.config.option.key"));
            sinkHeaderCellText(getText("report.config.option.description"));
            if (!optionWithDetail) {
                sinkHeaderCellText(getText("report.config.option.type"));
            }
            sinkHeaderCellText(getText("report.config.option.defaultValue"));

            if (!optionWithDetail) {
                sinkHeaderCellText(getText("report.config.option.final"));
                sinkHeaderCellText(getText("report.config.option.transient"));
            }
            sink.tableRow_();

            for (ConfigOptionDef option : options) {

                sink.tableRow();
                if (optionWithDetail) {
                    sinkCellLink(
                            option.getKey(),
                            "detail_" + configProvider.getName() + "_" + option.getKey());
                } else {
                    sinkCellText(option.getKey());
                }
                sinkCellText(l(locale, option.getDescription()));
                if (!optionWithDetail) {
                    sinkCellText(option.getType().getName());
                }
                sinkCellVerbatimText(getDefaultValue(option));
                if (!optionWithDetail) {
                    sinkCellText(getText(!option.isFinal()));
                    sinkCellText(getText(!option.isTransient()));
                }
                sink.tableRow_();
            }

            sink.table_();

        }
    }

    protected void renderActionDefsTable(ConfigActionDef... actions) {

        if (actions.length == 0) {

            sink.paragraph();
            sink.bold();
            sink.text(getText("report.detail.actions.noActions"));
            sink.bold_();
            sink.paragraph_();

        } else {

            sink.table();

            sink.tableRow();
            sinkHeaderCellText(getText("report.config.action.action"));
            sinkHeaderCellText(getText("report.config.action.aliases"));
            sink.tableRow_();

            for (ConfigActionDef action : actions) {
                sink.tableRow();
                sinkCellText(action.getAction());
                sinkCellText(l(locale, Arrays.toString(action.getAliases())));
                sink.tableRow_();
            }

            sink.table_();
        }
    }

    protected void renderProviderOptionDetail(ApplicationConfigProvider configProvider,
                                              ConfigOptionDef option) {

        final SinkEventAttributes cellWidth = new SinkEventAttributeSet();
        cellWidth.addAttribute(SinkEventAttributes.WIDTH, "80%");

        final SinkEventAttributes headerWidth = new SinkEventAttributeSet();
        headerWidth.addAttribute(SinkEventAttributes.WIDTH, "20%");
//        final String cellWidth = "80%";
//        final String headerWidth = "20%";

        sink.section4();
        sink.sectionTitle4();
        sink.anchor("detail_" + configProvider.getName() + "_" + option.getKey());
        sink.text(getText("report.config.option.detail") + "  '" + option.getKey()
                  + "'");
        sink.sectionTitle4_();
        sink.lineBreak();

        sink.table();
        sink.tableRows(new int[]{Sink.JUSTIFY_RIGHT, Sink.JUSTIFY_LEFT}, false);

        sink.tableRow();
        sinkHeaderCellText(headerWidth, getText("report.config.option.key"));
        sink.tableCell(cellWidth);
        sink.nonBreakingSpace();
        sink.bold();
        sink.text(option.getKey());
        sink.bold_();
        sink.tableCell_();
        sink.tableRow_();

        sink.tableRow();
        sinkHeaderCellText(headerWidth, getText("report.config.option.description"));
        sink.tableCell(cellWidth);
        sink.nonBreakingSpace();
        sink.text(l(locale, option.getDescription()));
        sink.tableCell_();
        sink.tableRow_();

        sink.tableRow();
        sinkHeaderCellText(headerWidth, getText("report.config.option.defaultValue"));
        sink.tableCell(cellWidth);
        sink.nonBreakingSpace();
        sink.bold();
        sink.text(getDefaultValue(option));
        sink.bold_();
        sink.tableCell_();
        sink.tableRow_();

        sink.tableRow();
        sinkHeaderCellText(headerWidth, getText("report.config.option.type"));
        sink.tableCell(cellWidth);
        sink.nonBreakingSpace();
        sink.text(option.getType().getName());
        sink.tableCell_();
        sink.tableRow_();

        sink.tableRow();
        sinkHeaderCellText(headerWidth, getText("report.config.option.final"));
        sink.tableCell(cellWidth);
        sink.nonBreakingSpace();
        sink.text(getText(!option.isFinal()));
        sink.tableCell_();
        sink.tableRow_();

        sink.tableRow();
        sinkHeaderCellText(headerWidth, getText("report.config.option.transient"));
        sink.tableCell(cellWidth);
        sink.nonBreakingSpace();
        sink.text(getText(!option.isTransient()));
        sink.tableCell_();
        sink.tableRow_();

        sink.table_();

        sink.paragraph();
        sinkLinkToAnchor(getText("report.back.options.table"), "detail_options_" + configProvider.getName());
        sink.paragraph_();

        sink.section4_();

    }

    protected String getText(boolean key) {
        return getText("report." + String.valueOf(key));
    }

    /**
     * Gets the localized message for this report.
     *
     * @param key the message key.
     * @return the message.
     */
    protected String getText(String key) {
        return i18n.getString(bundleName, locale, key);
    }

    protected String getDefaultValue(ConfigOptionDef option) {
        String defaultValue = option.getDefaultValue();
        if (StringUtils.isBlank(defaultValue)) {
            defaultValue = getText("report.noDefaultValue");
        }
        return defaultValue;
    }

    protected void renderWarningIcon() {
        sink.figure();
        sink.figureGraphics("images/icon_warning_sml.gif");
        sink.figure_();
    }

    protected void renderErrorIcon() {
        sink.figure();
        sink.figureGraphics("images/icon_error_sml.gif");
        sink.figure_();
    }

    protected void renderSuccessIcon() {
        sink.figure();
        sink.figureGraphics("images/icon_success_sml.gif");
        sink.figure_();
    }

    protected void renderInfoIcon() {
        sink.figure();
        sink.figureGraphics("images/icon_info_sml.gif");
        sink.figure_();
    }

    protected void sinkHeaderCellText(String text) {
        sink.tableHeaderCell();
        sink.text(text);
        sink.tableHeaderCell_();
    }

    protected void sinkHeaderCellText(SinkEventAttributes width, String text) {
        sink.tableHeaderCell(width);
        sink.text(text);
        sink.tableHeaderCell_();
    }

    protected void sinkCellText(SinkEventAttributes width, String text) {
        sink.tableCell(width);
        sink.text(text);
        sink.tableCell_();
    }

    protected void sinkCellText(String text) {
        sink.tableCell();
        sink.text(text);
        sink.tableCell_();
    }

    protected void sinkCellVerbatimText(String text) {
        sink.tableCell();
        sink.verbatim(SinkEventAttributeSet.MONOSPACED);
        sink.text(text);
        sink.verbatim_();
        sink.tableCell_();
    }

    protected void sinkCellLink(String text, String url) {
        sink.tableCell();
        sinkLinkToAnchor(text, url);
        sink.tableCell_();
    }

    protected void sinkLinkToAnchor(String text, String anchor) {
        sink.link("./" + reportName + ".html#" + anchor);
        sink.text(text);
        sink.link_();
    }

    protected Collection<ConfigOptionDef> getOptions(
            ApplicationConfigProvider configProvider) {
        List<ConfigOptionDef> result =
                new ArrayList<ConfigOptionDef>(
                        Arrays.asList(configProvider.getOptions()));
        Collections.sort(result, new Comparator<ConfigOptionDef>() {
            @Override
            public int compare(ConfigOptionDef o1,
                               ConfigOptionDef o2) {
                return o1.getKey().compareTo(o2.getKey());
            }
        });
        return result;
    }
}
