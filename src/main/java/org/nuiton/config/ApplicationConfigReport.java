package org.nuiton.config;

/*
 * #%L
 * Nuiton Maven Report Plugin
 * %%
 * Copyright (C) 2012 - 2016 CodeLutin, Tony Chemit
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.plugins.annotations.ResolutionScope;

import java.util.HashSet;
import java.util.List;

/**
 * Generates a report for declarated application config via the
 * {@link ApplicationConfigProvider} mecanism.
 *
 * For each configuration, you can find all his options and actions.
 *
 * @author Tony - chemit@codelutin.com
 * @since 2.6.10
 */
@Mojo(name = "config-report",
      requiresProject = true, requiresReports = true,
      requiresDependencyResolution = ResolutionScope.RUNTIME)
public class ApplicationConfigReport extends AbstractApplicationConfigReport {

    /**
     * List of all class-path elements.
     *
     * @since 2.6.10
     */
    @Parameter(property = "project.runtimeClasspathElements",
               required = true, readonly = true)
    protected List<String> runtimeClasspathElements;

    @Override
    protected ClassLoader createClassLoader() {
        return createClassLoader(new HashSet<String>(runtimeClasspathElements));
    }

}
